FROM registry.access.redhat.com/ubi9/ubi-minimal:latest@sha256:104cf11d890aeb7dd5728b7d7732e175a0e4018f1bb00d2faebcc8f6bf29bd52

RUN microdnf install -y findutils

COPY app /app

USER 1001

ENTRYPOINT ["/app/check-cache.sh"]
