#!/usr/bin/env bash

# Exit immediately on uninitialized variable or error, and print each command.
set -uex

# Mount files to be watched at /data.
cd /data

# Get list of changed files.
changed_files="$(md5sum --check --quiet '.checksums.md5' | sed -e 's/: [A-Z]*//')"

# Generate md5sums if theere are changes.
if [ -n "${changed_files}" ] || [ ! -f '.checksums.md5' ]; then
  find -- * -type f -exec md5sum {} + > '.checksums.md5'
fi

# If a file has changed, purge it.
[ -n "${changed_files}" ] || exit 0
while read -r line; do
  curl -i -X PURGE "${SERVICE_URL}${line}" -H "Fastly-Key: ${FASTLY_TOKEN}" -H "Accept: application/json"
done <<< "${changed_files}"
